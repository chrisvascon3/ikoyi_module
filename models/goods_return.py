from datetime import datetime, timedelta
import time
from dateutil.relativedelta import relativedelta

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_is_zero, float_compare
from odoo.tools.misc import formatLang
from odoo.addons.base.res.res_partner import WARNING_MESSAGE, WARNING_HELP
import odoo.addons.decimal_precision as dp
from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import ValidationError
from odoo import http

'''# GRO REQUEST WORKFLOW --
view and print grn, 
upload, 
select the Stock id,
system restrict to  the product in MOF
Selects how many to return
send to manager &
sends mail to and others
system deducts in inventory
credit not and debit note created,
send mail to vendor

'''
class Goods_Return(models.Model):
    _name = "ikoyi.goods_return"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Ikoyi Goods Retunr"
    _order = "id desc"

    def get_url(self, id, name):
        base_url = http.request.env['ir.config_parameter'].sudo(
        ).get_param('web.base.url')
        base_url += '/web# id=%d&view_type=form&model=%s' % (id, name)
        return "<a href={}> </b>Click<a/> to Review. ".format(base_url)
        
    '''def get_url(self, id, model):
        base_url = http.request.env['ir.config_parameter'].sudo(
        ).get_param('web.base.url')
        base_url += '/web# id=%d&view_type=form&model=%s' % (id, model)'''
        #  base_url += '/web# id=%d&view_type=form&model=%s' % (self.id,
        #  self._name)

        #  self.get_url(self.id, self._name)

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search(
            [('user_id', '=', self.env.uid)], limit=1)

    def default_partner_id(self):
        partner = self.env['res.partner'].browse([0])
        return partner.id
    #  @api.depends('employee_id')

    
    # USED TO GET LIST OF USERS IN THE GROUP CATEGORY
    # And then append them as followers
    
    def _get_all_followers(self):
        followers = []
        groups = self.env['res.groups']
        store_manager = groups.search([('name', 'ilike', "Store Manager")])
        store_keeper = groups.search([('name', 'ilike', "Store Keeper")])

        procure_manager = groups.search([('name', '=', "Procurement Manager")])
        procurement_officer = groups.search(
            [('name', '=', "Procurement Officer")])
        internal_control = groups.search(
            [('name', '=', "Internal Control Manager")])
        finance = groups.search([('name', '=', "Finance & Admin Manager")])
        chairman = groups.search([('name', '=', "Chairman")])
        vice_chairman = groups.search([('name', '=', "Vice Chairman")])
        general_manager = groups.search([('name', '=', "General Manager")])

        for rec in store_manager:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in store_keeper:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in procure_manager:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in procurement_officer:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in internal_control:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in finance:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in chairman:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in vice_chairman:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        for rec in general_manager:
            for users in rec.users:
                employee = self.env['hr.employee'].search(
                    [('user_id', '=', users.id)])
                for rex in employee:
                    followers.append(rex.id)

        return followers
    
    READONLY_STATES = {
        'store_manager': [('readonly', True)],
        'update': [('readonly', True)],
        'account': [('readonly', True)],
        'manager_two': [('readonly', True)],
        'to_vendor': [('readonly', True)],
        
    }

    name = fields.Char(
        'Order Reference',
        required=True,
        index=True,
        copy=False,
        default='New')
    # # # # partner_ref = fields.Char('Vendor Reference', copy=False)
    origin = fields.Char('Source Document', compute="Get_Stock", copy=False,)
    
    currency_id = fields.Many2one(
        'res.currency',
        'Currency',
        required=True,
        states=READONLY_STATES,
        default=lambda self: self.env.user.company_id.currency_id.id)
    
    stock_id = fields.Many2one(
        'stock.picking',
        string="Stock Move (GRN) ID", 
        required=True,copy=False,
        )

    order_line = fields.One2many(
        'good.return.line', 'order_id', string='Goods Return Lines', copy=True, store=True, readonly = False, compute="Get_Stock")
    
    user_id = fields.Many2one(
        'res.users',
        string="Users", readonly=True,
        default=lambda a: a.env.user.id)

    date_order = fields.Datetime(
        'Order Date',
        required=True,
        states=READONLY_STATES,
        index=True,
        copy=False,
        default=fields.Datetime.now)
    
    
    users_followers = fields.Many2many(
        'hr.employee',
        string='Add followers',
        required=False,
        default=_get_all_followers)
    
    branch_id = fields.Many2one(
        'res.branch',
        string="Section",
        default=lambda self: self.env.user.branch_id.id, required=True,
        help="Tell Admin to set your branch... (Users-->Preferences-->Allowed Branch)",compute="Get_Product_of_Stock")
    
    
    location = fields.Many2one(
        'stock.location',
        string = 'Stock Location', required=True,compute="Get_Product_of_Stock",
        help="Go to inventory config, set branch for warehouse and location")
     
    purchase_order_id = fields.Many2one(
        comodel_name="purchase.order",
        string='Purchase Order', copy=False,compute="Get_Product_of_Stock")
    
    file_upload = fields.Binary('File Upload', 
    required=True, 
    )
    binary_fname = fields.Char('Binary Name')

    company_id = fields.Many2one(
        'res.company',
        'Company',
        required=True,
        index=True,
        states=READONLY_STATES,
        default=lambda self: self.env.user.company_id.id)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('store_manager', 'Manager'),
        ('update', 'In Progress'),
        ('to_vendor', 'Waiting Availability'),
        ('manager_two', 'Manager'),
        ('account', 'Account Payable'),
        ('done', 'Done'),
        ('refuse', 'Refuse'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')

    employee_id = fields.Many2one(
        'hr.employee',states=READONLY_STATES,
        string='Employee',
        required=True,
        default=_default_employee)
    partner_id = fields.Many2one(
        'res.partner',
        string='Vendor',
        default=default_partner_id,
        track_visibility='always',compute="Get_Product_of_Stock")
    notes = fields.Text('Terms and Conditions')
    total_amount = fields.Float('Total Items to Return',compute="get_total")
    description_two = fields.Text('Refusal Reasons')
    
    @api.depends('stock_id')
    @api.one
    def Get_Product_of_Stock(self):
        po_obj = self.env['purchase.order']
        if self.stock_id:
            po_search = po_obj.search([('name','ilike',self.stock_id.origin)])
            po_id = 0    
                    # po = po_obj.browse(po_search.id)
            if po_search:
                po_id =po_search.id
            else:
                raise ValidationError('No PO found for the GRN')
                    
        for rec in self:
            for tec in rec.stock_id:
                rec.update({'location':tec.location_id.id, 
                            'origin':tec.origin,
                            'branch_id':tec.branch_id.id,
                            'purchase_order_id':po_id,
                            'partner_id':po_search.partner_id.id,})
         
    
    @api.one
    @api.depends('order_line')
    def get_total(self):
        for rec in self.order_line:
            self.total_amount += rec.qty
        
            
    @api.depends('stock_id')
    @api.one
    def Get_Stock(self):
        
        prod_ids = self.stock_id.pack_operation_product_ids
        stock_id = self.stock_id
        for prod_id in prod_ids:
            lines = {'order_id':self.id,'product_id':prod_id.product_id.id,'qty':0,'initial_qty':prod_id.qty_done,
                     'location':prod_id.location_dest_id}
            self.order_line =  [(0, 0, lines)]
                                
            
            
    #################################################
    
    @api.multi
    def send_to_manager(self): # draft officer
        self.write({'state':'store_manager','description_two':''})
        body = "Dear Sir, <br/>A GRO with reference Number: {} have been sent for confirmation.\
            Kindly {} to view. <br/>\
            Regards".format(self.name,self.get_url(self.id, self._name))
        return self.send_mail_manager(body)
    
    @api.multi
    def manager_approve(self): # store_manager manager
        self.write({'state':'update','description_two':''})
        body = "Dear Sir, <br/>A GRO with reference Number: {} have been confirmed and will be returned to the vendor:\
             {}. Also an Credit and Debit Note have been generated by the Store.\
            Kindly {} to view. <br/>\
            Regards".format(self.name, self.partner_id.name,self.get_url(self.id, self._name))
        return self.send_mail_all(body)
    
    @api.multi
    def storer_send_vendor(self):
        self.write({'state':'to_vendor','description_two':''})
        self.Quantity_Moves()
        return self.send_mail_vendor()
    
    
    def view_grn(self):
        """View GRN"""
        self.ensure_one()
        report = self.env['ir.actions.report.xml'].search([('report_name','=', 'ikoyi_module.edit_grn_gro_ikoyi')])
        if report:
            report.write({'report_type':'qweb-html'})
        return self.env['report'].get_action(self.id, 'ikoyi_module.edit_grn_gro_ikoyi')
    

    @api.multi
    def print_credit_note(self): # to_vendor
        return self.env['report'].get_action(self, 'ikoyi_module.edit_grn_gro_ikoyi')
    
    @api.multi
    def print_debit_note(self): # to_vendor
        return self.env['report'].get_action(self, 'ikoyi_module.edit_grn_gro_ikoyi')
    
    # ########################### REJECT ##############################
    @api.multi
    def button_rejects(self):  #  store_manager,manager_two,
        if not self.description_two:
            raise ValidationError(
                'Please Add a Remark in the Refusal Note tab below')
        else:
            if state == "store_manager":
                self.state = "draft"
                
            elif state == "manager_two":
                self.state = "to_vendor"
            
             
    ############################# RECEIVING GOOD ######################
    @api.multi
    def storer_officer_receive(self): # to_vendor  officer
        self.write({'state':'manager_two'})
        body = "Dear Sir, <br/>We wish to notify you that prior to GRO with Number: {}, \
        the vendor: {} have returned the goods and we have confirmed them.\
            Kindly {} to approve. <br/>\
            Regards".format(self.name,self.partner_id.name,self.get_url(self.id,self._name))
            
        return self.send_mail_manager(body)
    
        
    @api.multi
    def manager_two_approve(self): # manager_two  manager
        self.write({'state':'account'})
        body = "Dear Sir, <br/>Prior to the GRO with reference Number: {} the goods have been returned by the vendor:\
             {}. Also GRN have been generated by the Store.\
            Kindly {} to view. <br/>\
            Regards".format(self.name, self.partner_id.name,self.get_url(self.id, self._name))
        self.create_picking()
        return self.send_mail_all(body)
    
    # 3###################### MAIL FUNCTIONS ########################
    
    def send_mail_manager(self, body):
        email_from = self.env.user.email
        group_user_id2 = self.env.ref('ikoyi_module.inventory_manager_ikoyi').id
        group_user_id = self.env.ref('ikoyi_module.store_keeper_ikoyi').id
        group_user_id3 = self.env.ref('ikoyi_module.procurement_officer_ikoyi').id

        if self.id:
            bodyx = body
            self.mail_sending_for_three(
                email_from,
                group_user_id,
                group_user_id2,
                group_user_id3,
                bodyx)
             
        else:
            raise ValidationError('No Record created')
    
    def send_mail_all(self,body):
        email_from = self.env.user.email
        group_user_id2 = self.env.ref('ikoyi_module.costing_manager_ikoyi').id
        group_user_id = self.env.ref('ikoyi_module.account_boss_ikoyi').id
        group_user_id3 = self.env.ref('ikoyi_module.internal_control_ikoyi').id

        if self.id:
            bodyx = body
            self.mail_sending_for_three(
                email_from,
                group_user_id,
                group_user_id2,
                group_user_id3,
                bodyx)
             
        else:
            raise ValidationError('No Record created')
        
        
    @api.multi
    def send_mail_vendor(self, force=False):
        email_from = self.env.user.company_id.email
        mail_to = self.partner_id.email
        subject = "Ikoyi Club GRO Notification"
        bodyx = "This is to notify you that some goods will be returned to you on {} because of some reasons \
        </br> For further enquires,\
         kindly contact {} </br> {} </br>\
        Thanks".format(fields.Date.today(), self.env.user.company_id.name, self.env.user.company_id.phone)
        self.mail_sending_one(email_from, mail_to, bodyx, subject)
        
    def mail_sending_one(self, email_from, mail_to, bodyx, subject):
        for order in self:
            mail_tos = str(mail_to)
            email_froms = "Ikoyi Club " + " <" + str(email_from) + ">"
            subject = subject
            mail_data = {
                'email_from': email_froms,
                'subject': subject,
                'email_to': mail_tos,
                #  'email_cc':,#  + (','.join(str(extra)),
                'reply_to': email_from,
                'body_html': bodyx
            }
            mail_id = order.env['mail.mail'].create(mail_data)
            order.env['mail.mail'].send(mail_id)

    def mail_sending_for_three(
            self,
            email_from,
            group_user_id,
            group_user_id2,
            group_user_id3,
            bodyx):
        from_browse = self.env.user.name
        groups = self.env['res.groups']
        for order in self:
            group_users = groups.search([('id', '=', group_user_id)])
            group_users2 = groups.search([('id', '=', group_user_id2)])
            group_users3 = groups.search([('id', '=', group_user_id3)])
            group_emails = group_users.users
            group_emails2 = group_users2.users
            group_emails3 = group_users3.users

            append_mails = []
            append_mails_to = []
            append_mails_to3 = []
            for group_mail in group_emails:
                append_mails.append(group_mail.login)

            for group_mail2 in group_emails2:
                append_mails_to.append(group_mail2.login)

            for group_mail3 in group_emails3:
                append_mails_to3.append(group_mail3.login)

            all_mails = append_mails + append_mails_to + append_mails_to3
            print all_mails
            email_froms = str(from_browse) + " <" + str(email_from) + ">"
            mail_sender = (', '.join(str(item)for item in all_mails))

            #mail_appends = (', '.join(str(item)for item in append_mails))
            #mail_appends_to = (', '.join(str(item)for item in append_mails_to))
            subject = "Goods Return Notification"
            bodyxx = "Dear Sir/Madam, <br/>We wish to notify you that a GRO from {} has been sent to you for approval <br/> <br/>Kindly review it. </br> <br/>Thanks".format(
                self.employee_id.name)
            mail_data = {
                'email_from': email_froms,
                'subject': subject,
                'email_to': mail_sender,
                'email_cc': mail_sender,  # + (','.join(str(extra)),
                'reply_to': email_from,
                'body_html': bodyx
            }
            mail_id = order.env['mail.mail'].create(mail_data)
            order.env['mail.mail'].send(mail_id)
            
                    
    '''@api.onchange('location')
    def domain_product_location(self):
        domain = {}
        products = []
        for rec in self:
            stock_location = self.env['stock.location']
            search_location = stock_location.search(
                [('branch_id', '=', rec.branch_id.id)])
            for r in search_location:
                stock_quant = self.env['stock.quant']
                search_quanty = stock_quant.search(
                    [('location_id', '=', r.id)])
                for ref in search_quanty:

                    products.append(ref.product_id.id)
                    domain = {'product_id': [('id', 'in', products)]}
            return {'domain': domain}'''

    
    def Quantity_Moves(self):
        diff = 0.0
        stock_location = self.env['stock.location']
        stock_quant = self.env['stock.quant']
        product_return_moves = []
        
        for line in self.order_line:
            if line.location and line.product_id:
                picking = self.env['stock.picking'].browse(self.stock_id.id)#(self.env.context.get('active_id'))
   
                moves = [move.id for move in picking.move_lines]
                locate = self.env['stock.location'].search([('usage','=','supplier')])
                  
                stock_create = stock_quant.create({
                                                    'product_id': line.product_id.id,
                                                    'qty': -line.qty,
                                                    'location_id': locate[0].id,
                                                    'in_date': self.date_order,
                                                    'move.history_ids':[(4,moves)]
                                                    })
                    
            else:
                raise ValidationError('Cannot find any Location')
        
    def create_picking(self):
        picking = self.env['stock.picking']
        locate_in = self.env['stock.location'].search([('usage','=','internal')])
        locate_out = self.env['stock.location'].search([('usage','=','supplier')])
        
        warehouse = self.env['stock.warehouse'].search(
            [('branch_id', '=', self.env.user.branch_id.id)])
        
        domain = [
                    ('code', '=', 'incoming'),
                    ('warehouse_id', '=', warehouse[0].id),
                    ('active', '=', True),
                    ('default_location_dest_id', '=', locate_in.id)
                ]

        picking_type_id = self.env['stock.picking.type'].search(domain)
        picking_type_id2 = self.env['stock.picking.type'].search(
                    [('active', '=', True)])[0]
        
            
        value_obj = {'partner_id':self.partner_id.id,
                  'min_date':self.date_order,
                  'location_id':locate_out[0].id,
                  'location_dest_id':locate_in[0].id,
                  'branch_id':self.branch_id.id,
                  'picking_type_id': picking_type_id2.id,
                  
                  }
        pick_id = picking.create(value_obj)
        pick_browse = picking.browse(pick_id.id)
                                
        for line in self.order_line:
            lines = {'product_id':line.product_id.id,
                     'qty_done':line.receive_qty,
                     'product_qty':line.qty,
                     'location_id':locate_out[0].id,
                     'location_dest_id':locate_in[0].id
                     }
        
            pick_browse.write({'pack_operation_product_ids':[(0,0, lines)]})
        pick_browse.action_confirm()
        # self.origin = pick_browse.name
        
        form_view_ref = self.env.ref('stock.view_picking_form', False)
        tree_view_ref = self.env.ref('stock.vpicktree', False)

        return {
            'domain': [('id', '=', pick_id.id)],
            'name': 'Stock Picking',
            'res_model': 'stock.picking',
            'type': 'ir.actions.act_window',
            #  'views': [(form_view_ref.id, 'form')],
            'views': [(form_view_ref.id, 'form'),(tree_view_ref.id, 'tree')],
            #'search_view_id': search_view_ref and search_view_ref.id,
        }
    
        #return self.see_breakdown_invoice()
    
     
    def see_breakdown_invoice(self,source):  #  vis_account,

         
        form_view_ref = self.env.ref('stock.view_picking_form', False)
        tree_view_ref = self.env.ref('stock.vpicktree', False)

        return {
            'domain': [('id', '=', source)],
            'name': 'Stock Picking',
            'res_model': 'stock.picking',
            'type': 'ir.actions.act_window',
            #  'views': [(form_view_ref.id, 'form')],
            'views': [(form_view_ref.id, 'form'),(tree_view_ref.id, 'tree')],
            #'search_view_id': search_view_ref and search_view_ref.id,
        }
    
    @api.constrains('order_line')
    def check_qty(self):
        for rec in self.order_line:
            if self.state == 'draft':
                if rec.qty < 0:
                    raise ValidationError('You must define the number of product to return in the lines must be greater than 0')
                else:
                    pass
            elif self.state == 'to_vendor':
                if rec.receive_qty < 0:
                    raise ValidationError('You must define the number of product received in the lines must be greater than 0')
                else:
                    pass
                #rec.write({'qty':1})                             
                    
class Goods_return_Line(models.Model):
    _name = "good.return.line"
    _description = 'Goods return Line'
    _order = 'id desc'
    _rec_name = "product_id"

    def change_uom(self):
        uom = self.env['product.uom'].search([('name', 'ilike', 'Unit(s)')])
        return uom.id


    order_id = fields.Many2one(
        'ikoyi.goods_return',
        string='Reference',
        index=True,
         
        ondelete='cascade')

    product_id = fields.Many2one(
        'product.product',
        string='Product',
        change_default=True,
        required=True)
    
    label = fields.Many2one(
        'product.uom',
        string='UOM',
        default=change_uom,
        required=False)
    qty = fields.Float('Qty to Return', required=True )
    
    initial_qty = fields.Float('Initial Received Qty', required=False )
    receive_qty = fields.Float('Received Qty', required=False )
    
    location = fields.Many2one(
        'stock.location',
        string = 'Stock Location', required=True,
        help="Go to inventory config, set branch for warehouse and location")
     
    
    
    
    
'''class Send_Main_Sendback(models.Model):
    _name = "main.return"

    resp = fields.Many2one(
        'res.users',
        'Responsible',
        default=lambda self: self.env.user.id)  # , default=self.write_uid.id)
    number = fields.Integer('Number ID')
    # <tree string="Memo Payments" colors="red:state == 'account';black:state == 'manager';green:state == 'coo';grey:state == 'refused';">
    reason = fields.Text('Reason', required=True)
    date = fields.Datetime('Date')
    state = fields.Char('State')
    users_followers = fields.Many2many('hr.employee', string='Add followers')

    @api.multi
    def post_back(self):
        #############################
        #state = 'refuse'
        model = 'requisition.inventory'

        #############################
        search = self.env['requisition.inventory'].search(
            [('id', '=', self.number)])
        message = "Memo Rejected at {} stage - Remarks: {}".format(
            str(search.state).capitalize().replace('_', ' '), self.reason)
        if search:
            search.write({'reason': message})
        else:
            raise ValidationError('Not found')
        return{'type': 'ir.actions.act_window_close'}




'''