#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Maduka Sopulu Chris kingston
#
# Created:     20/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#--------------------------------------------------------------------
{
    'name': 'Ikoyi Club ERP',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description': """ERP Application for managing
                     the whole activities of ikoyi club""",
    'category': 'Procurement',

    'depends': ['base', 'purchase', 'branch', 'stock', 'hr'],
    'data': [
        'security/security_group.xml',
        'security/menu_views_security.xml',
        'views/ikoyi_procurement_views.xml',
        'views/hr_inherit.xml',
        'views/requisition_view.xml',
        'views/goods_return_views.xml',
        'reports/grn_note.xml',
        'reports/siv_note.xml',
        'security/ir.model.access.csv',
    ],
    'price': 4200.99,
    'currency': 'USD',


    'installable': True,
    'auto_install': False,
}
